INSERT INTO books (best_reading_time, published, title, genre)
VALUES ('23:59:53', '1983-06-09', 'climate change', 'NON_FICTION');
INSERT INTO books (best_reading_time, published, title, genre)
VALUES ('19:24:23', '1942-12-05', 'the scary night', 'THRILLER');
INSERT INTO books (best_reading_time, published, title, genre)
VALUES ('01:04:18', '1942-12-22', 'how to fly', 'NON_FICTION');
INSERT INTO books (best_reading_time, published, title, genre)
VALUES ('17:22:38', '1947-11-21', 'the martian', 'SCIFI');
INSERT INTO books (best_reading_time, published, title, genre)
VALUES ('09:07:20', '1990-04-16', 'help me', 'THRILLER');
INSERT INTO books (best_reading_time, published, title, genre)
VALUES ('23:53:08', '1969-09-12', 'there she goes', 'THRILLER');
