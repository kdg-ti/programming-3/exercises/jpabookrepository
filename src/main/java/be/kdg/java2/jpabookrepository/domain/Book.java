package be.kdg.java2.jpabookrepository.domain;

import jakarta.persistence.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Random;

@Entity
@Table(name="BOOKS")
public class Book {
    public enum Genre {
        THRILLER, SCIFI, NON_FICTION
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(nullable = false, length = 50)
    private String title;
    private LocalDate published;
    private LocalTime bestReadingTime;
    @Enumerated(EnumType.STRING)
    private Genre genre;

    protected Book(){}

    public Book(String title, LocalDate published, LocalTime bestReadingTime, Genre genre) {
        this.title = title;
        this.published = published;
        this.bestReadingTime = bestReadingTime;
        this.genre = genre;
    }

    public static Book randomBook(){
        Random random = new Random();
        return new Book("title"+random.nextInt(),
                LocalDate.ofEpochDay(random.nextLong(-10000,10000)),
                LocalTime.ofNanoOfDay(random.nextLong(24L*60*60*1000000000)),
                Genre.values()[random.nextInt(Genre.values().length)]);
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public LocalDate getPublished() {
        return published;
    }

    public LocalTime getBestReadingTime() {
        return bestReadingTime;
    }

    public Genre getGenre() {
        return genre;
    }

    //setters zijn niet nodig voor JPA
    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPublished(LocalDate published) {
        this.published = published;
    }

    public void setBestReadingTime(LocalTime bestReadingTime) {
        this.bestReadingTime = bestReadingTime;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", published=" + published +
                ", bestReadingTime=" + bestReadingTime +
                ", genre=" + genre +
                '}';
    }
}
