package be.kdg.java2.jpabookrepository.service;

import be.kdg.java2.jpabookrepository.domain.Book;

import java.util.List;

public interface BookService {
    List<Book> findAll();

    Book findById(int id);

    Book createRandomBook();

    void deleteBook(int id);

    void updateBook(Book book);

}
