package be.kdg.java2.jpabookrepository.service;

import be.kdg.java2.jpabookrepository.domain.Book;
import be.kdg.java2.jpabookrepository.repository.BookRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {
    private BookRepository repository;

    public BookServiceImpl(BookRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Book> findAll(){
        //TODO
        return null;
    }

    @Override
    public Book findById(int id){
        //TODO
        return null;
    }

    @Override
    public Book createRandomBook() {
        //TODO, yu can use Book.randomBook method...
        return null;
    }

    @Override
    public void deleteBook(int id) {
        //TODO
    }

    @Override
    public void updateBook(Book book){
        //TODO
    }
}
