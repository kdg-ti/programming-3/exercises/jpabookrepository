package be.kdg.java2.jpabookrepository.presentation;

import be.kdg.java2.jpabookrepository.domain.Book;
import be.kdg.java2.jpabookrepository.repository.BookRepository;
import be.kdg.java2.jpabookrepository.service.BookService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class Menu implements CommandLineRunner {
    private BookService service;
    private Scanner scanner = new Scanner(System.in);

    public Menu(BookService service) {
        this.service = service;
    }

    public void run(String... args){
        while (true) {
            System.out.println("1) Show books");
            System.out.println("2) Add a random book");
            System.out.println("3) Update title of book");
            System.out.println("4) Delete book");
            System.out.print("Enter:");
            int choice = scanner.nextInt();
            scanner.nextLine();
            switch (choice) {
                case 1 -> showBooks();
                case 2 -> addRandomBook();
                case 3 -> updateTitleOfBook();
                case 4 -> deleteBook();
            }
        }
    }

    private void showBooks(){
        //TODO
    }

    private void addRandomBook() {
        //TODO
    }

    private void updateTitleOfBook() {
        System.out.print("Book id:");
        int id = scanner.nextInt();
        scanner.nextLine();
        System.out.print("New title:");
        String title = scanner.nextLine();
        //TODO: update the title of this book
    }

    private void deleteBook(){
        System.out.print("Book id:");
        int id = scanner.nextInt();
        scanner.nextLine();
        //TODO: delete this book
    }
}
