package be.kdg.java2.jpabookrepository;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpabookrepositoryApplication {

    public static void main(String[] args) {
        SpringApplication.run(JpabookrepositoryApplication.class, args);
    }

}
