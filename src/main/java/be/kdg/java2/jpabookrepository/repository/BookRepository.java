package be.kdg.java2.jpabookrepository.repository;

import be.kdg.java2.jpabookrepository.domain.Book;

import java.util.List;

public interface BookRepository {
    List<Book> findAll();
    Book findById(int id);
    Book create(Book book);
    void update(Book book);
    void delete(Book book);
}
