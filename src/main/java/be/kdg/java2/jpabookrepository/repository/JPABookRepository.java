package be.kdg.java2.jpabookrepository.repository;

import be.kdg.java2.jpabookrepository.domain.Book;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class JPABookRepository implements BookRepository {

    @Override
    public List<Book> findAll() {
        //TODO
        return null;
    }

    @Override
    public Book findById(int id) {
        //TODO
        return null;
    }

    @Override
    public Book create(Book book) {
        //TODO
        return null;
    }

    @Override
    public void update(Book book) {
        //TODO
    }

    @Override
    public void delete(Book book) {
        //TODO
    }
}
